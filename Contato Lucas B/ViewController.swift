//
//  ViewController.swift
//  Contato Lucas B
//
//  Created by COTEMIG on 13/01/44 AH.
//

import UIKit

struct Contato {
    let nome: String
    let numero: String
    let email: String
    let endereco: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDeContatos:[Contato] = []
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContatos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as!MyCell
        let contato = listaDeContatos[indexPath.row]
        
        cell.nome.text = contato.nome
        cell.numero.text = contato.numero
        cell.email.text = contato.email
        cell.endereco.text = contato.endereco
        
        return cell
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        
        listaDeContatos.append(Contato(nome: "John Appleseed", numero: "(888 555-1212)", email: "john@mac.com", endereco: "1345 Laurel Street"))
        listaDeContatos.append(Contato(nome: "Kate Bell", numero: "(222 124-1212)", email: "kate@mac.com", endereco: "043 David Street"))
        listaDeContatos.append(Contato(nome: "Anna Haro", numero: "(555 987-1212)", email: "anna@mac.com", endereco: "69 Lucas Street"))
        listaDeContatos.append(Contato(nome: "David Taylo", numero: "(666 121-1313)", email: "david@mac.com", endereco: "012 David Street"))
        
    }


}

